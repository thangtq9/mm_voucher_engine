﻿namespace Voucher.API.Model
{
    public class LogElasticModel
    {
        public string user_name { get; set; }
        public string end_point { get; set; }
        public string header { get; set; }
        public string param { get; set; }
        public string body { get; set; }
        public string customer_code { get; set; }
        public string voucher_code { get; set; }
        public string request_time { get; set; }
        public int status_code { get; set; }
        public string response { get; set; }
        public string response_time { get; set; }
        public int timestamp_mili { get; set; }
        public DateTime timestamp { get; set; }
        public string v_tran_id { get; set; }
    }
}
