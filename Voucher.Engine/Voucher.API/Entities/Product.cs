﻿namespace Voucher.API.Entities
{
    public class Product
    {
        public int ProductId { get; set; } = 0;
        public string ProductName { get; set; } = "";
        public string ProductDescription { get; set; } = "";
        public int ProductPrice { get; set; } = 0;
        public int ProductStock { get; set; } = 0;
    }
}
