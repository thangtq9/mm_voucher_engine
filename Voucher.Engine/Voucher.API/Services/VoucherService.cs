﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using Voucher.API.Context;
using Voucher.API.Entities;
using Voucher.API.Repositories;

namespace Voucher.API.Services
{
    public class VoucherService : IVoucherService
    {

        private readonly DbContextClass _dbContext;
        public VoucherService(DbContextClass dbContext)
        {
            _dbContext = dbContext;
        }

        public DataTable GetVoucherByCustomer(string customer_id, string VoucherCode)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@CustomerId", customer_id));
            parameter.Add(new SqlParameter("@VoucherCode", VoucherCode));
            DataTable dt = new DataTable();
            dt = _dbContext.ExecuteDataTable("GetVoucherByCusId", 120, parameter.ToArray());
            return dt;
        }
    }
}
