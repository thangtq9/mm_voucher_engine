﻿using System.Data;
using Voucher.API.Entities;
namespace Voucher.API.Repositories
{
    public interface IVoucherService
    {
        public DataTable GetVoucherByCustomer(string customer_id, string VoucherCode);
    }
}
