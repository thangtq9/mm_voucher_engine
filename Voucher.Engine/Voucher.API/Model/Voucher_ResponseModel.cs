﻿using static ServiceStack.LicenseUtils;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel.DataAnnotations;

namespace Voucher.API.Model
{
    public class Voucher_ResponseModel
    {
        public string v_tran_id { get; set; } = "";
        public string status_code { get; set; }
        public string msg { get; set; } = "";
        //public decimal voucher_actual { get; set; } =0;
        public decimal? discount { get; set; }
        public Order_detail_Response[] item { get; set; }
        public LGVoucherModel v_info { get; set; }
    }
    public class Order_detail_Response
    {
        [Range(1, 1000, ErrorMessage = "Value sq_index for {0} must be between {1} and {2}.")]
        public int sq_index { get; set; }
        [StringLength(32, ErrorMessage = "The article_no value cannot exceed 32 characters. "), NotNull]
        public string article_no { get; set; } = "";
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal amt_in_vat { get; set; }
        public bool use_in_voucher { get; set; }
        public string supplier_code { get; set; } = "";
    }
}
