﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using Voucher.API.Context;
using Voucher.API.Entities;
using Voucher.API.Repositories;

namespace Voucher.API.Services
{
    /// <summary>
    /// Article Server.
    /// </summary>
    public class ArticleService : IArticleService
    {
        private readonly DbContextClass _dbContext;
        /// <summary>
        /// Init
        /// </summary>
        public ArticleService(DbContextClass dbContext)
        {
            _dbContext = dbContext;
        }
        /// <summary>
        /// Get Productlist by List Article, store no.Requid
        /// </summary>
        public async Task<List<Article>> GetProductListAsync(string ListArticleNo, string store_no)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@ListArticleNo", ListArticleNo));
            parameter.Add(new SqlParameter("@store_no", store_no));

            var productDetails = await Task.Run(() => _dbContext.Article
                           .FromSqlRaw(@"exec GetArticleByArticleNo @ListArticleNo @store_no", parameter.ToArray()).ToListAsync());
            return productDetails;
        }
        public DataTable GetArticleList(string listarticle, string list_own_brand, string list_supplier_brand, string list_supplier)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@listarticle", listarticle));
            parameter.Add(new SqlParameter("@list_own_brand", list_own_brand));
            parameter.Add(new SqlParameter("@list_supplier_brand", list_supplier_brand));
            parameter.Add(new SqlParameter("@list_supplier", list_supplier));
            DataTable dt = new DataTable();
            dt = _dbContext.ExecuteDataTable("GetArticleList_LG", 5, parameter.ToArray());
            return dt;
        }
        public DataTable GetVoucherByCustomer(string customer_id, string VoucherCode)
        {
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@CustomerId", customer_id));
            parameter.Add(new SqlParameter("@VoucherCode", VoucherCode));
            DataTable dt = new DataTable();
            dt = _dbContext.ExecuteDataTable("GetVoucherByCusId", 120, parameter.ToArray());
            return dt;
        }
    }
}
