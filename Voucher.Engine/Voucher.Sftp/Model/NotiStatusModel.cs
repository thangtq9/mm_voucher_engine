﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voucher.Sftp.Model
{
    public class DataBSM
    {
        public string trace_id { get; set; }
    }

    public class MetaDataBSM
    {
        public bool is_success { get; set; }
        public string message { get; set; }
    }

    public class NotificationBsmModel
    {
        public MetaDataBSM meta_data { get; set; }
        public DataBSM data { get; set; }
    }

    public class NotiStatusModel
    {
        public NotiStatusModel(string _customer_code, string _noti_status)
        {
            this.customer_code = _customer_code;
            this.noti_status = noti_status;
        }

        public string customer_code { get; set; }
        public string noti_status { get; set; }
    }
}
