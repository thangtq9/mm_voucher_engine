﻿using Microsoft.Extensions.Configuration;
using System.Data.Common;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

namespace Voucher.Sftp
{
    public class DbContextClass : DbContext
    {
        protected readonly IConfiguration Configuration;

        public DbContextClass(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            String connectstring = Configuration.GetConnectionString("DefaultConnection");
            if (string.IsNullOrEmpty(connectstring))
            {
                string env_netcore = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                if (string.IsNullOrEmpty(env_netcore))
                {
                    env_netcore = "Development";
                }
                IConfigurationRoot configuration1 = new ConfigurationBuilder()
              .SetBasePath(AppContext.BaseDirectory)
              .AddJsonFile("appsettings." + env_netcore + ".json")
              .Build();
                connectstring = configuration1.GetConnectionString("DefaultConnection");
            }
            options.UseSqlServer(connectstring);
        }

        public DataTable ExecuteDataTable(string procedureName, int commandTimeoutInSeconds, params SqlParameter[] parameters)
        {
            var dataTable = new DataTable();
            using (var connection = this.Database.GetDbConnection())
            {
                try
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = commandTimeoutInSeconds;

                        using (var adapter = new SqlDataAdapter((SqlCommand)command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    System.GC.Collect();
                }
            }
            return dataTable;
        }
        public int ExecuteNonQuery(string procedureName, int commandTimeoutInSeconds, params SqlParameter[] parameters)
        {
            using (var connection = this.Database.GetDbConnection())
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandTimeout = 300;
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = commandTimeoutInSeconds;
                        command.Transaction = transaction;
                        int var = command.ExecuteNonQuery();
                        transaction.Commit();
                        return var;
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                    }
                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                        System.GC.Collect();
                    }
                }
            }
        }
    }
}
