﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using Renci.SshNet;
using RestSharp;
using System;
using System.Data;
using System.Net.Security;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Voucher.Sftp.Helps;
using Voucher.Sftp.Model;
using System.Numerics;
using System.Web;
using static System.Net.WebRequestMethods;
using Voucher.Jobs;

namespace Voucher.Sftp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string env_netcore = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (string.IsNullOrEmpty(env_netcore))
            {
                env_netcore = "Development";
            }
            IConfiguration configuration = new ConfigurationBuilder()
            //.SetBasePath(AppContext.BaseDirectory)
            .AddJsonFile(AppContext.BaseDirectory + "appsettings." + env_netcore + ".json")
            .Build();
            Console.WriteLine("BaseDirectory:" + AppContext.BaseDirectory);
            Console.WriteLine("ENV:" + env_netcore);

            string Monitor = configuration["SftpConfig:Monitor"];
            string host = configuration["SftpConfig:Host"];
            string username = configuration["SftpConfig:Username"];
            string password = configuration["SftpConfig:Password"];

            string remoteInboundFolderPath = configuration["SftpConfig:Inbound"];
            string localDataFolderPath = AppContext.BaseDirectory + "SFTP\\";// configuration["SftpConfig:LocalFile"];

            if (!Directory.Exists(localDataFolderPath))
                Directory.CreateDirectory(localDataFolderPath);

            string remoteBackupFolderPath = configuration["SftpConfig:Backup"];


            #region 7h15 sáng get file csv
            string time_get_csv = configuration["Schedule:GetCSV"];
            if (DateTime.Now.Hour.ToString() == time_get_csv)
            {
                string file_name_log = DateTime.Now.ToString("yyyyMMdd") + "_getcsv";
                try
                {
                    Console.WriteLine("Start Connect SFTP:" + host + ":" + username + ":" + password);
                    WriteLog(file_name_log, "Start Connect SFTP:" + host + ":" + username + ":" + password, "Logs");
                    using (var client = new SftpClient(host, username, password))
                    {
                        client.Connect();
                        Console.WriteLine("Start Connect sucessfull;");
                        WriteLog(file_name_log, "Start Connect sucessfull;", "Logs");
                        // Lấy danh sách tệp trong thư mục SFTP

                        Console.WriteLine("Start Get List Directory;");
                        WriteLog(file_name_log, "Start Get List Directory;", "Logs");
                        var files = client.ListDirectory(remoteInboundFolderPath);

                        if (files != null && files.Count() > 0)
                        {
                            Console.WriteLine("Get List Directory done:" + files.Count() + " file");
                            WriteLog(file_name_log, "Start Get List Directory:" + files.Count() + " file", "Logs");
                            foreach (var file in files)
                            {
                                if (!file.IsDirectory)
                                {
                                    Console.WriteLine("Start Connect file " + file.Name);
                                    string issue_date = file.Name.Replace("vouchersDaily_", "").Replace(".csv", "");
                                    WriteLog(file_name_log, "Start Connect file " + file.Name + ";issuedate:" + issue_date, "Logs");
                                    string remoteFilePath = remoteInboundFolderPath + "/" + file.Name;// Path.Combine(remoteInboundFolderPath, file.Name);
                                    string localFilePath = localDataFolderPath + file.Name;// Path.Combine(localDataFolderPath, file.Name);
                                    string remoteBackupFilePath = remoteBackupFolderPath + "/" + file.Name;// Path.Combine(remoteBackupFolderPath, file.Name);

                                    using (Stream fileStream = System.IO.File.OpenWrite(localFilePath))
                                    {
                                        client.DownloadFile(remoteFilePath, fileStream);
                                    }
                                    Console.WriteLine("Download file " + file.Name + " sucessfull");
                                    WriteLog(file_name_log, "Download file " + file.Name + " sucessfull", "Logs");


                                    //Read file cvs to datatable
                                    DataTable dt = ReadFileCSV(localFilePath, 1);

                                    Console.WriteLine("Read file " + file.Name + " sucessfull:" + dt.Rows.Count + " items");
                                    WriteLog(file_name_log, "Read file " + file.Name + " sucessfull:" + dt.Rows.Count + " items", "Logs");

                                    var d1 = dt.Rows.Count;
                                    if (d1 > 0)
                                    {
                                        int totalRows = dt.Rows.Count;
                                        int batchSize = 1000;

                                        for (int i = 0; i < totalRows; i += batchSize)
                                        {
                                            DataTable partialTable = GetPartialDataTable(dt, i, batchSize);
                                            // Xử lý dữ liệu trong partialTable tại đây
                                            int va = Voucher_Save(partialTable, issue_date, configuration);
                                        }
                                        //int va = Voucher_Save(dt, configuration);

                                    }

                                    // Di chuyển tệp từ thư mục cục bộ vào thư mục SFTP sau khi tải xong
                                    using (var stream = System.IO.File.OpenRead(localFilePath))
                                    {
                                        client.UploadFile(stream, remoteBackupFilePath);
                                    }
                                    Console.WriteLine("Move file " + file.Name + " to folder backup sucessfull");
                                    WriteLog(file_name_log, "Move file " + file.Name + " to folder backup sucessfull", "Logs");


                                    // Xóa tệp gốc trên máy chủ SFTP
                                    client.DeleteFile(remoteFilePath);
                                    Console.WriteLine("Delete file " + file.Name + " in folder inbound sucessfull");
                                    WriteLog(file_name_log, "Delete file " + file.Name + " in folder inbound sucessfull", "Logs");


                                    Console.WriteLine("Save db Done");
                                    WriteLog(file_name_log, "Save db Done", "Logs");
                                }
                                else
                                {
                                    Console.WriteLine("Folder Name :" + file.Name);
                                    WriteLog(file_name_log, "Folder Name :" + file.Name, "Logs");
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("No file");
                            WriteLog(file_name_log, "No file", "Logs");
                        }
                        client.Disconnect();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:" + ex.Message);
                    WriteLog(file_name_log, "Error:" + ex.Message, "Logs");
                    try
                    {
                        var result_sms = send_sms<SMSResponse>();
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            #endregion

            #region 9h00 sáng gửi notification
            if (DateTime.Now.Hour.ToString() == configuration["Schedule:SendNotification"])
            {
                string file_name_log = DateTime.Now.ToString("yyyyMMdd") + "_send_noti";
                WriteLog(file_name_log, "Start Send Notification", "Logs");
                try
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("customer_code", typeof(string));
                    dt.Columns.Add("noti_status", typeof(string));
                    dt.Columns.Add("noti_msg", typeof(string));

                    using (DataTable dt_noti = Voucher_Need_Notification(configuration))
                    {
                        if (dt_noti != null && dt_noti.Rows.Count > 0)
                        {
                            int total_roww_noti = dt_noti.Rows.Count;
                            Console.WriteLine("dt_noti:" + dt_noti.Rows.Count.ToString());
                            WriteLog(file_name_log, "dt_noti:" + dt_noti.Rows.Count.ToString(), "Logs");
                            foreach (DataRow dr_noti in dt_noti.Rows)
                            {
                                WriteLog(file_name_log, dr_noti["customerCode"].ToString(), "Logs");
                                string bu = dr_noti["bu"].ToString() ?? "";
                                string customerCode = dr_noti["customerCode"].ToString();

                                DataRow dr = dt.NewRow();
                                string customer_code = dr_noti["customerCode"].ToString();
                                dr["customer_code"] = customer_code;

                                #region bsm
                                if (bu.ToLower() == "bsm")
                                {
                                    HttpResponseMessage response = await UtilitiesW.SendNotiBcard(customerCode, configuration["Notification:bcard_noti_endpiont"]);
                                    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                                    {
                                        try
                                        {
                                            NotificationBsmModel rootdc = JsonConvert.DeserializeObject<NotificationBsmModel>(await response.Content.ReadAsStringAsync());
                                            if (rootdc != null && rootdc.meta_data != null && rootdc.meta_data.is_success == true)
                                            {
                                                dr["noti_status"] = "Success";
                                                dr["noti_msg"] = rootdc.meta_data.message;
                                            }
                                            else
                                            {
                                                dr["noti_status"] = "Failed";
                                                if (rootdc != null && rootdc.meta_data != null)
                                                    dr["noti_msg"] = rootdc.meta_data.message;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            dr["noti_status"] = "Failed";
                                            dr["noti_msg"] = ex.Message;
                                        }

                                    }
                                    else
                                    {
                                        dr["noti_status"] = "Failed";
                                        dr["noti_msg"] = response.Content;
                                    }
                                }
                                #endregion

                                #region mm
                                if (bu.ToLower() == "mm")
                                {
                                    HttpResponseMessage response = await UtilitiesW.SendNotiMCard(customerCode, configuration["Notification:mm_noti_endpiont"]);
                                    if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
                                    {
                                        try
                                        {
                                            NotificationMMModel rootdc = JsonConvert.DeserializeObject<NotificationMMModel>(await response.Content.ReadAsStringAsync());
                                            if (rootdc != null && rootdc.meta_data != null && rootdc.meta_data.status == 1)
                                            {
                                                dr["noti_status"] = "Success";
                                                dr["noti_msg"] = rootdc.meta_data.message;
                                            }
                                            else
                                            {
                                                dr["noti_status"] = "Failed";
                                                if (rootdc != null && rootdc.meta_data != null)
                                                    dr["noti_msg"] = rootdc.meta_data.message;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            dr["noti_status"] = "Failed";
                                            dr["noti_msg"] = ex.Message +"/" + await response.Content.ReadAsStringAsync();
                                        }
                                    }
                                    else
                                    {
                                        dr["noti_status"] = "Failed";
                                        if (response != null)
                                            dr["noti_msg"] = response.Content;
                                    }
                                }
                                #endregion

                                dt.Rows.Add(dr);
                                if (dt != null && dt.Rows.Count > 100)
                                {
                                    int value = Voucher_Noti_Action(dt, configuration);
                                    Console.WriteLine("Noti Save Log DB Done:" + dt.Rows.Count + ". Còn lại: " + total_roww_noti);
                                    total_roww_noti = total_roww_noti - dt.Rows.Count;
                                    WriteLog(file_name_log, "Noti Save Log DB Done:" + dt.Rows.Count + ". Còn lại: " + total_roww_noti, "Logs");
                                    WriteLog(file_name_log, "----------------------------------------------------", "Logs");
                                    dt = new DataTable();
                                    dt.Columns.Add("customer_code", typeof(string));
                                    dt.Columns.Add("noti_status", typeof(string));
                                    dt.Columns.Add("noti_msg", typeof(string));
                                    Thread.Sleep(10000);
                                }

                            }
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                int value = Voucher_Noti_Action(dt, configuration);
                                Console.WriteLine("Noti Save Log DB Done:" + dt.Rows.Count);
                                WriteLog(file_name_log, "Noti Save Log DB Done:" + dt.Rows.Count, "Logs");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Data empty");
                            WriteLog(file_name_log, "Data empty", "Logs");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:" + ex.Message);
                    WriteLog(file_name_log, "Error:" + ex.Message, "Logs");
                    try
                    {
                        var result_sms = send_sms<SMSResponse>();
                    }
                    catch (Exception)
                    {
                    }
                }

            }

            #endregion

            if (Monitor == "1")
            {
                Console.ReadLine();
            }

        }


        public static string oddosession_id = "90c5bbeee82e492bb64923c6bd24067f708978b8";
        public static T send_sms<T>()
        {
            try
            {
                string encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
                               .GetBytes("bsmart" + ":" + "Sms@Bsmart040$"));
                var sms_message = "000000" + " la ma OTP dang nhap vao ung dung BCard cua B-S MART";
                string phone = "84908185626";
                string input = @"Src=" + HttpUtility.UrlEncode("B-S MART") + "&Des=" + phone + "&Message=" + HttpUtility.UrlEncode(sms_message) + "&AppName=BCard";
                ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };
                using (WebClient client = new WebClient())
                {
                    var settings = new JsonSerializerSettings { StringEscapeHandling = StringEscapeHandling.EscapeHtml };
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    client.Headers.Add("api_key", "9d65582876671eac5ad81795976ee5c73c5a1f89");
                    client.Headers.Add("X-Openerp", "9d65582876671eac5ad81795976ee5c73c5a1f89");
                    client.Headers.Add("mm-key", "f1d18b5ce37b927f11000b051a810f0ec0c6c8e8847047327426413c11d32bea");
                    if (!string.IsNullOrEmpty(encoded))
                        client.Headers.Add("Authorization", "Basic " + encoded);
                    client.Headers.Add("Cookie", "session_id=" + oddosession_id);
                    var result = JsonConvert.DeserializeObject<T>(client.UploadString("http://sms.metrosmsmgt.com/apismsservice", "POST", input));
                    return result;
                }
            }
            catch (Exception ex)
            {
                return default(T);
            }

        }

        public static DataTable GetPartialDataTable(DataTable originalTable, int startIndex, int rowCount)
        {
            DataTable newTable = originalTable.Clone(); // Tạo một bảng mới với cùng cấu trúc

            int endIndex = Math.Min(startIndex + rowCount, originalTable.Rows.Count);

            for (int i = startIndex; i < endIndex; i++)
            {
                newTable.ImportRow(originalTable.Rows[i]);
            }

            return newTable;
        }
        public static DataTable ReadFileCSV(string filePath, int row_column)
        {
            // Create a TextFieldParser object to read the file
            using (TextFieldParser parser = new TextFieldParser(filePath))
            {
                // Set the delimiter for the fields in the CSV file
                parser.Delimiters = new string[] { ";" };

                // Skip over header row
                //parser.ReadLine();

                // Loop through each line in the file
                int index = 1;
                DataTable data = new DataTable();
                int NumberOfCulumns = 0;
                while (!parser.EndOfData)
                {
                    // Read the fields on the current line and do something with them
                    string[] fields = parser.ReadFields();
                    if (index == row_column)
                    {
                        fields.ToList().ForEach(f => data.Columns.Add(f, typeof(string)));
                        NumberOfCulumns += fields.Length;
                    }
                    else if (index > row_column)
                    {
                        if (fields.Length > 0 && NumberOfCulumns == fields.Length)
                        {
                            DataRow dr = data.NewRow();
                            for (int i = 0; i < fields.Length; i++)
                            {
                                string scientificNotation = fields[i];
                                dr[i] = scientificNotation;
                            }
                            data.Rows.Add(dr);
                        }
                    }
                    index++;
                }
                return data;
            }
        }

        public static int Voucher_Save(DataTable dt, string issue_date, IConfiguration configuration)
        {
            DbContextClass _dbContext = new DbContextClass(configuration);
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@dt", dt));
            parameter.Add(new SqlParameter("@issue_date", issue_date));
            parameter.Add(new SqlParameter("@BU", "EMPTY"));
            return _dbContext.ExecuteNonQuery("[dbo].[Voucher.Import]", 120, parameter.ToArray());
        }

        public static DataTable Voucher_Need_Notification(IConfiguration configuration)
        {
            DbContextClass _dbContext = new DbContextClass(configuration);
            var parameter = new List<SqlParameter>();
            return _dbContext.ExecuteDataTable("[dbo].[Voucher.Need.Notification]", 120, parameter.ToArray());
        }

        public static int Voucher_Noti_Action(DataTable dt, IConfiguration configuration)
        {
            DbContextClass _dbContext = new DbContextClass(configuration);
            var parameter = new List<SqlParameter>();
            parameter.Add(new SqlParameter("@dt", dt));
            return _dbContext.ExecuteNonQuery("[dbo].[Voucher.Noti.Action]", 120, parameter.ToArray());
        }

        public static void WriteLog(string filename, string Log, string folder)
        {
            FileStream _stream = null;
            StreamWriter sw = null;
            try
            {
                string Path = AppContext.BaseDirectory + "/" + filename + ".log";
                if (!string.IsNullOrEmpty(folder))
                {
                    if (!Directory.Exists(AppContext.BaseDirectory + "/" + folder))
                        Directory.CreateDirectory(AppContext.BaseDirectory + "/" + folder);
                    Path = AppContext.BaseDirectory + "/" + folder + "/" + filename + ".log";
                }

                if (!System.IO.File.Exists(Path))
                    _stream = System.IO.File.Create(Path);
                else
                {
                    FileInfo file = new FileInfo(Path);
                    if (file.Length > 20000000)
                    {
                        file.Delete();
                        _stream = System.IO.File.Create(Path);
                    }
                    else
                        _stream = new FileStream(Path, FileMode.Append);
                }
                sw = new StreamWriter(_stream);
                sw.Write("***" + DateTime.Now.ToString() + Environment.NewLine + Log + Environment.NewLine);
                sw.Flush();
            }
            catch (Exception ex)
            {
            }
            if (sw != null)
            {
                sw.Close();
                sw.Dispose();
            }
            if (_stream != null)
            {
                _stream.Close();
                _stream.Dispose();
            }
        }
    }
}