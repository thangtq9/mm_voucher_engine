﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.Metadata;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Voucher.Sftp.Helps
{
    public static class UtilitiesW
    {
        //public RestResponse ApiToObject(string endpoint, Dictionary<string, string> header, object jsonbody, Dictionary<string, string> parametter, RestSharp.Method method = RestSharp.Method.Post, string user_login = "MMVN")
        //{
        //    try
        //    {
        //        var client = new RestClient(endpoint);
        //        var request = new RestRequest("", method);
        //        //request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        //        //request.AddHeader("Accept", "application/json");
        //        request.AddHeader("Content-Type", "application/json");
        //        if (header != null && header.Count > 0)
        //        {
        //            foreach (var item in header)
        //            {
        //                request.AddHeader(item.Key, item.Value);
        //            }
        //        }
        //        if (parametter != null && parametter.Count > 0)
        //        {
        //            foreach (var item in parametter)
        //            {
        //                request.AddParameter(item.Key, item.Value);
        //            }
        //        }
        //        if (jsonbody != null)
        //        {
        //            request.AddJsonBody(jsonbody);
        //        }

        //        RestResponse response = client.Execute(request);
        //        return response;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public static async Task<HttpResponseMessage> SendNotiBcard(string Customer_id, string url = "http://172.26.16.163")
        {
            try
            {
                var client = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Post, url + "/Notifications/SendNotiByCard");
                request.Headers.Add("accept", "*/*");
                var content = new StringContent("{\r\n    \"data\": {\r\n        \"title_vn\": \"Thông báo\",\r\n        \"title_en\": \"Notification\",\r\n        \"content_vn\": \"Bsmart tặng bạn phiếu giảm giá. Vui lòng sử dụng thẻ BCard của bạn với mã phiếu này khi bạn mua hàng tại cửa hàng Bsmart.\",\r\n        \"content_en\": \"Bsmart offer you discount coupon. Please use your BCard card with this coupon code when you make purchases at Bsmart.\",\r\n        \"card_no\": \"" + Customer_id + "\",\r\n        \n        \"sent_by_uname\": \"LG_GURU\",\r\n        \"firebaseData\": {\r\n            \"navigator\": \"my_gifts\"\r\n        }\r\n    }\r\n}", null, "application/json");
                request.Content = content;
                client.Timeout = TimeSpan.FromSeconds(10);
                var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                return response;
                //return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static async Task<HttpResponseMessage> SendNotiMCard(string Customer_id, string url = "https://merchant-api.mmvietnam.app")
        {
            try
            {
                var client = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Post, url + "/loyalguru/push-noti");
                request.Headers.Add("Authorization", "Basic bG95YWxndXJ1Ompoc2RmaDkyaDNyODcyaDNybjJrZG4yMzg3cmgzNDg=");
                var content = new StringContent("{\n    \"customer_id\": \"" + Customer_id + "\",\n    \"title\": {\n        \"default\": \"Notification\",\n        \"en\": \"Notification\",\n        \"vi\": \"Thông báo\"\n    },\n    \"content\": {\n        \"default\": \"MM Mega Market tặng bạn phiếu giảm giá. Vui lòng sử dụng thẻ MCard của bạn với mã phiếu này khi bạn mua hàng tại kho MM Mega Market.\",\n        \"en\": \"MM Mega Market offer you discount coupon. Please use your MCard card with this coupon code when you make purchases at MM Mega Market.\",\n        \"vi\": \"MM Mega Market tặng bạn phiếu giảm giá. Vui lòng sử dụng thẻ MCard của bạn với mã phiếu này khi bạn mua hàng tại kho MM Mega Market.\"\n    }\n}", null, "application/json");
                request.Content = content;
                client.Timeout =  TimeSpan.FromSeconds(10);
                var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                return response;
                //return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static async Task<HttpResponseMessage> SendNotiCCOD(string Customer_id, string url = "https://merchant-api.mmvietnam.app")
        {
            try
            {

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                HttpClient client = new HttpClient(clientHandler);

                var request = new HttpRequestMessage(HttpMethod.Post, "https://172.26.16.107:5000/api/customer/findByCriteria");
                request.Headers.Add("x-api-key", "6r7vlPtpmHhk0xRaZNQbd5pQLIXSzF75");
                //request.Headers.Add("x-req-sig", "2661d297b9ccee8bb9420c3f8916b97303c042fcb50a8401b30628c2b49564b3");
                var content = new StringContent("{\n   \"customer_id\": null,\n   \"customer_no\": null,\n   \"customer_name\": null,\n   \"customer_status\": null,\n   \"customer_addr\": null,\n   \"created_date_from\": \"2021-01-01\",\n   \"created_date_to\": \"2026-12-03\",\n   \"card_no\": null,\n   \"customer_category_id\": null,\n   \"class_level1\": null,\n   \"class_level2\": null,\n   \"class_level3\": null,\n   \"class_level4\": null,\n   \"bu_code\": null,\n   \"home_store_code\": null,\n   \"delivery_store_code\": \"\",\n   \"is_cod\": null,\n   \"is_credit_customer\": null,\n   \"is_pbd\": null,\n   \"is_parent\": null,\n   \"customer_email\":null,\n   \"customer_telephone\":null,\n   \"card_email\":null,\n   \"card_telephone\":null,\n   \"card_mobile\":\"0786270392\",\n   \"page_no\" : 1,\n   \"limit\": 20,\n   \"sort_by\" : [\n    {\n        \"column\":\"customer_name\",\n        \"direction\" : \"asc\"\n    },\n    {\n        \"column\":\"customer_no\",\n        \"direction\" : \"desc\"\n    },\n    {\n        \"column\":\"home_store\",\n        \"direction\" : \"desc\"\n    }\n   ]\n}", null, "application/json");
                request.Content = content;
                string hash = "";
                using (HMACSHA256 hmac = new HMACSHA256(Encoding.UTF8.GetBytes("skey_uat_6D919F4381819")))
                {
                    byte[] hashBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(content.ReadAsStringAsync().Result));
                    hash = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
                }

                request.Headers.Add("x-req-sig", hash);
                var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string CalculateHMACSHA256(string data, string key)
        {
            using (HMACSHA256 hmac = new HMACSHA256(Encoding.UTF8.GetBytes(key)))
            {
                byte[] hashBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(data));
                return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
            }
        }

        public static async Task<HttpResponseMessage> SendNotiBcardV2(string Customer_id, string url = "http://172.26.16.163")
        {
            try
            {
                var request1 = new 
                {
                    data = new 
                    {
                        title_en="Thong báo",
                        title_vn= "Thong báo",
                        content_en= "Cam ơn ban đã mua hàng tại BSmart",
                        content_vn= "Cam ơn ban đã mua hàng tại BSmart",
                        card_no= "2223000045190011",
                        firebaseData= new
                        {
                            navigator= "my_gifts"
                        },
                    }
                };

                string jsonData = JsonConvert.SerializeObject(request1);
                var client = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Post, url + "/Notifications/SendNotiByCard");
                request.Headers.Add("accept", "*/*");
                var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                request.Content = content;
                var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                return response;
                //return await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public class Title
        {
            public string @default { get; set; }
            public string en { get; set; }
            public string vi { get; set; }
        }

        public class Content
        {
            public string @default { get; set; }
            public string en { get; set; }
            public string vi { get; set; }
        }

        public class NotificationRequest
        {
            public string customer_id { get; set; }
            public Title title { get; set; }
            public Content content { get; set; }
        }


        public class Data
        {
            public string title_vn { get; set; }
            public string title_en { get; set; }
            public string content_vn { get; set; }
            public string content_en { get; set; }
            public string card_no { get; set; }
            public string sent_by_uname { get; set; }
            public FirebaseData firebaseData { get; set; }
        }

        public class FirebaseData
        {
            public string navigator { get; set; }
        }

        public class Root
        {
            public Data data { get; set; }
        }


    }
}
