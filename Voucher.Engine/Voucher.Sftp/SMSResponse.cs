﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voucher.Jobs
{
    public class SMSResponse
    {
        public string src { get; set; }
        public string des { get; set; }
        public string message { get; set; }
        public int status { get; set; }
        public string transactionId { get; set; }
        public int smsCount { get; set; }
        public string network { get; set; }
        public int length { get; set; }
        public int cost { get; set; }
        public string sms_message { get; set; }
        public string appname { get; set; }
    }
}
