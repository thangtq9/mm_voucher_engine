﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voucher.Sftp.Model
{
    public class MetaData
    {
        public string request_id { get; set; }
        public int status { get; set; }
        public string message { get; set; }
    }

    public class NotificationMMModel
    {
        public MetaData meta_data { get; set; }
        public bool? data { get; set; }
    }
}
