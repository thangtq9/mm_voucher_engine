﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Voucher.API.Entities
{
    public class VoucherInfo
    {
        [Key]
        public string customer_code { get; set; } = "";
        public int available_redemptions { get; set; } = 0;
        public int frozen_redemptions { get; set; } =0;
        public string affectation { get; set; } = "";
    }
}
