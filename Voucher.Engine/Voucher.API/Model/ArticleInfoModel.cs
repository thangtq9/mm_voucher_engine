﻿namespace Voucher.API.Model
{
    public class ArticleInfoModel
    {
        public int Article_code_01 { set; get; }
        public string? Designation_product { set; get; }
        public string? supplier_code { set; get; }
    }
}
