﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;
using System.Data;
using Voucher.API.Entities;


namespace Voucher.API.Context
{
    public class DbContextClass : DbContext
    {

        //public DbContextClass(DbContextOptions<DbContextClass> options) : base(options)
        //{
        //}
        protected readonly IConfiguration Configuration;

        public DbContextClass(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            string connections = Configuration.GetConnectionString("DefaultConnection");

            if (string.IsNullOrEmpty(connections))
            {
                IConfiguration configuration1 = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
            .AddJsonFile("appsettings." + Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") + ".json")
            .Build();
                connections = configuration1.GetConnectionString("DefaultConnection");
            }
            options.UseSqlServer(connections);
        }

        public DbSet<Product> Product { get; set; }
        public DbSet<Article> Article { get; set; }
        public DbSet<VoucherInfo> VoucherInfo { get; set; }

        public DataTable ExecuteDataTable(string procedureName, int commandTimeoutInSeconds, params SqlParameter[] parameters)
        {
            var dataTable = new DataTable();
            using (var connection = this.Database.GetDbConnection())
            {
                try
                {
                    if (string.IsNullOrEmpty(connection.ConnectionString))
                    {
                        IConfiguration configuration1 = new ConfigurationBuilder()
                        .SetBasePath(AppContext.BaseDirectory)
                    .AddJsonFile("appsettings." + Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") + ".json")
                    .Build();
                        connection.ConnectionString = configuration1.GetConnectionString("DefaultConnection");
                    }

                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = commandTimeoutInSeconds;

                        using (var adapter = new SqlDataAdapter((SqlCommand)command))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    System.GC.Collect();
                }
            }
            return dataTable;
        }

        public DataSet ExecuteDataSet(string procedureName, int commandTimeoutInSeconds, params SqlParameter[] parameters)
        {
            var dataSet = new DataSet();
            using (var connection = this.Database.GetDbConnection())
            {
                try
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = commandTimeoutInSeconds;

                        using (var adapter = new SqlDataAdapter((SqlCommand)command))
                        {
                            adapter.Fill(dataSet);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    System.GC.Collect();
                }
            }
            return dataSet;
        }
        public int ExecuteNonQuery(string procedureName, int commandTimeoutInSeconds, params SqlParameter[] parameters)
        {
            using (var connection = this.Database.GetDbConnection())
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                try
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = procedureName;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        command.CommandTimeout = commandTimeoutInSeconds;
                        command.Transaction = transaction;
                        int var = command.ExecuteNonQuery();
                        transaction.Commit();
                        return var;
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                    }
                    throw;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                        System.GC.Collect();
                    }
                }
            }
        }
    }
}
