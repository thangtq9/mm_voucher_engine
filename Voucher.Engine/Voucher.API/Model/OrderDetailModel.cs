﻿namespace Voucher.API.Model
{
    public class OrderDetailModel
    {
        public string article_no { get; set; } = "";
        public int qty { get; set; }
        public decimal amt_in_vat { get; set; }
        public string brand { get; set; }
        public string department { get; set; }
        public string supplier_code { get; set; }
    }
}
