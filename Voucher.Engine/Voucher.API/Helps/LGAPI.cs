﻿using Azure;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.VisualBasic;
using Nest;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Runtime.Intrinsics.Arm;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Voucher.API.Model;

namespace Voucher.API.Helps
{
    public class LGAPI
    {
        protected readonly IConfiguration Configuration;
        private String LG_EndPoint;
        private String LG_api_secret;
        private String LG_api_key;
        public LGAPI(IConfiguration configuration)
        {
            LG_EndPoint = GetAppSetting(configuration, "LG_EndPoint");
            LG_api_secret = GetAppSetting(configuration, "LG_api_secret");
            LG_api_key = GetAppSetting(configuration, "LG_api_key");
        }
        public static string GetAppSetting(IConfiguration configuration, string key)
        {
            string _key = "";
            try
            {
                return configuration["AppSettings:" + key].ToString();
            }
            catch (Exception ex)
            {
                _key = "-10";
            }
            return _key;
        }

        public async Task<RestResponse> GetListVoucher(string CustomerNo, string starting_after)
        {
            try
            {
                //ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var options = new RestClientOptions(LG_EndPoint)//("https://streaming.loyal.guru")
                {
                    MaxTimeout = -1
                };
                var client = new RestClient(options);
                var request = new RestRequest("/customers/" + CustomerNo + "/vouchers", Method.Get);
                //clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                request.AddHeader("X-Api-Secret", LG_api_secret);
                request.AddHeader("X-Api-Key", LG_api_key);

                //request.AddHeader("X-Api-Secret", "bWVnYW1hcmtldF92aWV0bmFtOjk2MWE3MTRiMjkyZjViZGE0YWIwMTg1NjRlODkxMWE4ODEwZTQxMDM=");
                //request.AddHeader("X-Api-Key", "5d7806e34c2beb116f6703216f68e250702c1b0508cb1bf1e02be4bf5c8e7a7d");
                request.AddHeader("Content-Type", "application/json ");

                if (!string.IsNullOrEmpty(starting_after) && starting_after != "start")
                    request.AddParameter("starting_after", starting_after);

                RestResponse response = await client.ExecuteAsync(request);
                return response;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<List<LGVoucherModel>> GetListVoucherAllPage(string CustomerNo)
        {
            try
            {
                List<LGVoucherModel>? l_voucher = new List<LGVoucherModel>();

                string? _next_token_starts_after = "start";
                while (!string.IsNullOrEmpty(_next_token_starts_after))
                {
                    RestResponse rp = await GetListVoucher(CustomerNo, _next_token_starts_after);
                    _next_token_starts_after = null;
                    if (rp != null)
                    {
                        if (rp.StatusCode == HttpStatusCode.OK)
                        {
                            if (!string.IsNullOrEmpty(rp.Content) && rp.Content != null)
                            {
                                VoucherRoot? vr = JsonConvert.DeserializeObject<VoucherRoot>(rp.Content);
                                if (vr != null)
                                {
                                    _next_token_starts_after = vr.next_token_starts_after;
                                    List<LGVoucherModel>? l_voucher_item = vr.data.ToList();
                                    if (l_voucher_item != null && l_voucher_item.Count > 0)
                                    {
                                        foreach (LGVoucherModel lv in l_voucher_item)
                                        {
                                            l_voucher.Add(lv);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return l_voucher;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<HttpResponseMessage> GetListVoucher1(string CustomerNo)
        {
            try
            {
                //ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var client = new HttpClient();
                var request = new HttpRequestMessage(HttpMethod.Get, "https://streaming.loyal.guru/customers/2219001304610018/vouchers");
                request.Headers.Add("X-Api-Secret", "bWVnYW1hcmtldF92aWV0bmFtOjk2MWE3MTRiMjkyZjViZGE0YWIwMTg1NjRlODkxMWE4ODEwZTQxMDM=");
                request.Headers.Add("X-Api-Key", "5d7806e34c2beb116f6703216f68e250702c1b0508cb1bf1e02be4bf5c8e7a7d");
                var content = new StringContent(string.Empty);
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json ");
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                request.Content = content;
                return await client.SendAsync(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<RestResponse> GetVoucherByLocation(string CustomerNo, string VoucherCode, string Location_Code)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var options = new RestClientOptions(LG_EndPoint)//("https://streaming.loyal.guru")
                {
                    MaxTimeout = -1
                };
                var client = new RestClient(options);
                var request = new RestRequest("/vouchers/" + VoucherCode + "?location_id=" + Location_Code + "&customer_id=" + CustomerNo, Method.Get);

                request.AddHeader("X-Api-Secret", LG_api_secret);
                request.AddHeader("X-Api-Key", LG_api_key);

                request.AddHeader("Content-Type", "application/json ");

                RestResponse response = await client.ExecuteAsync(request);
                return response;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public HttpResponseMessage HttpUploadFile()
        {
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            // Tạo một HttpClientHandler
            var handler = new HttpClientHandler();

            // Tắt kiểm tra chứng chỉ SSL (chỉ dùng cho môi trường không an toàn)
            handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, errors) => true;
            using (var client = new HttpClient(handler))
            {
                using (var content = new MultipartFormDataContent())
                {
                    var request = new HttpRequestMessage(HttpMethod.Get, "https://streaming.loyal.guru/customers/2219001304610018/vouchers");
                    request.Headers.Add("X-Api-Secret", "bWVnYW1hcmtldF92aWV0bmFtOjk2MWE3MTRiMjkyZjViZGE0YWIwMTg1NjRlODkxMWE4ODEwZTQxMDM=");
                    request.Headers.Add("X-Api-Key", "5d7806e34c2beb116f6703216f68e250702c1b0508cb1bf1e02be4bf5c8e7a7d");
                    var content1 = new StringContent(string.Empty);
                    //content.Headers.ContentType = new MediaTypeHeaderValue("application/json ");
                    client.DefaultRequestHeaders.Add("Accept", "application/json");
                    request.Content = content1;
                    return client.Send(request);

                    //var jsonContent = new StringContent(reqbody);
                    //jsonContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                    //content.Add(jsonContent, "documents_input");

                    //var fileContent = new StreamContent(File.OpenRead(filepath));
                    //fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                    //content.Add(fileContent, "files", filename);

                    //var response = client.PostAsync(requrl, content).Result;
                    //var responseContent = response.Content.ReadAsStringAsync().Result;

                }
            }

        }

        public List<Order_detail_Request> ListArticleInOrderActiveVoucher(List<Order_detail_Request> l_detail, List<int>? product_ids)
        {
            List<Order_detail_Request> l_detail_active = new List<Order_detail_Request>();
            if (product_ids != null && product_ids.Count > 0)
            {
                l_detail_active = l_detail.Where(item => product_ids.Contains(Convert.ToInt32(item.article_no))).ToList();
            }
            return l_detail_active;
        }

        public static string CalculateHMACSHA256(string data, string key)
        {
            using (HMACSHA256 hmac = new HMACSHA256(Encoding.UTF8.GetBytes(key)))
            {
                byte[] hashBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(data));
                return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
            }
        }
    }
}
