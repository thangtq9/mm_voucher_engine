﻿namespace Voucher.API.Model
{
    public  class VoucherRoot
    {
        public LGVoucherModel[] data;
        public string? next_token_starts_after;
    } 
        
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class LGVoucherModel
    {
        public int available_redemptions { get; set; }
        public int frozen_redemptions { get; set; }
        public string affectation { get; set; }
        public string available_from { get; set; }
        public string available_to { get; set; }
        public string discount_type { get; set; }
        public double discount { get; set; }
        public int max_redemptions { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public string image_original { get; set; }
        public int? min_units { get; set; }
        public int? max_units { get; set; }
        public int? min_money { get; set; }
        public int? max_money { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public List<int>? product_ids { get; set; }
        public int id { get; set; }
        public bool selected { get; set; }
        public bool cumulative { get; set; }
        public bool has_points { get; set; }
        public List<Feature>? features { get; set; }
        public string? supplier_code { get; set; }
    }

    public class Feature
    {
        public string taxonomy_slug { get; set; }
        public long id { get; set; }
    }


}
