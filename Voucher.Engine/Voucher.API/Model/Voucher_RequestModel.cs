﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Voucher.API.Model
{
    public class Voucher_RequestModel
    {
        [StringLength(32, ErrorMessage = "The customer_no value cannot exceed 32 characters. ")]
        //[Required(ErrorMessage = "The field customer_no is required.")]
        public string customer_no { get; set; }
        [StringLength(32, ErrorMessage = "The store_no value cannot exceed 32 characters. "), NotNull]
        public string store_no { get; set; }
        [StringLength(32, ErrorMessage = "The lg_number value cannot exceed 32 characters. "), NotNull]
        public string lg_number { get; set; }
        public Order_detail_Request[] item { get; set; }

    }
    public class Order_detail_Request
    {
        /// <summary>
        /// This property represents an integer value.
        /// Maximum value: 1000.
        /// Minimum value :1 
        /// </summary>
        [Range(1, 1000, ErrorMessage = "Value sq_index for {0} must be between {1} and {2}.")]
        public int sq_index { get; set; }
        [StringLength(32, ErrorMessage = "The article_no value cannot exceed 32 characters. "), NotNull]
        public string article_no { get; set; } = "";
        /// <summary>
        /// This property represents an integer value.
        /// Maximum value: 1000.
        /// Minimum value :1 
        /// </summary>
        public decimal qty { get; set; }
        public decimal price { get; set; }
        /// <summary>
        /// This property represents an integer value.
        /// Maximum value: 1.000.000.000.
        /// Minimum value :1000 
        /// </summary>
        public decimal amt_in_vat { get; set; }
    }
}
