﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Voucher.API.Entities
{
    public class Article
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int a_id { get; set; }
        [Required, StringLength(32)]
        public string Article_code_01 { get; set; } = "";
        [Required,StringLength(8)]
        public string Class_code_02 { get; set; } = "";
        [Required, StringLength(32), JsonIgnore]
        public string Supplier_code { get; set; } = "";
        public string Product_brand { get; set; } = "";
        public DateTime create_date { get; set; } = DateTime.Now;
    }
}
