﻿using System.Data;
using Voucher.API.Entities;

namespace Voucher.API.Repositories
{
    /// <summary>
    /// Interface ArticleService
    /// </summary>
    public interface IArticleService
    {
        /// <summary>
        /// Get Productlist by List Article, store no.Requid
        /// </summary>
        public Task<List<Article>> GetProductListAsync(string ListArticleNo,string store_no);
        public DataTable GetArticleList(string listarticle,string list_own_brand,string list_supplier_brand,string list_supplier);
        public DataTable GetVoucherByCustomer(string customer_id, string VoucherCode);
    }
}
