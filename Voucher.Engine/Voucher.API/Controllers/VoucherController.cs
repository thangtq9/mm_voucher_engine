﻿using Google.Apis.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Nest;
using Newtonsoft.Json;
using RestSharp;
using ServiceStack.Redis;
using System;
using System.Data;
using System.Net;
using System.Runtime.Intrinsics.Arm;
using Voucher.API.Helps;
using Voucher.API.Model;
using Voucher.API.Repositories;

namespace Voucher.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VoucherController : ControllerBase
    {
        private readonly IArticleService aService;
        protected readonly IConfiguration Configuration;
        private readonly ElasticSearch esearch;
        private readonly LGAPI lgapi;
        private readonly IRedisClientsManager _redisManager = null;
        private readonly IRedisClient _redisclient = null;

        public VoucherController(IArticleService articleService, ElasticSearch _esearch, LGAPI _LGAPI, IConfiguration configuration)
        {
            this.aService = articleService;
            Configuration = configuration;
            esearch = _esearch;
            lgapi = _LGAPI;
            if (_redisManager == null)
            {
                try
                {
                    RedisPoolConfig _config = new RedisPoolConfig();
                    _config.MaxPoolSize = 100;
                    if (string.IsNullOrEmpty(Configuration["AppSettings:RedisPassword"]))
                        _redisManager = new RedisManagerPool(Configuration["AppSettings:RedisIP"], _config);
                    else
                    {
                        var config = new RedisEndpoint
                        {
                            Host = Configuration["AppSettings:RedisIP"],
                            Port = 6379,
                            Password = Configuration["AppSettings:RedisPassword"],
                            ConnectTimeout = 1000,
                            ReceiveTimeout = 1000,
                            SendTimeout = 1000,
                            RetryTimeout = 500,
                        };
                        _redisManager = new RedisManagerPool(config.ToString(), _config);
                    }
                    _redisclient = _redisManager.GetClient();
                }
                catch (Exception)
                {
                }
            }
        }

        [HttpPost("calculate")]
        public async Task<ActionResult> calculate([FromBody] Voucher_RequestModel v_request)
        {
            LogElasticModel le = new LogElasticModel();
            le.end_point = "121212";
            le.body = JsonConvert.SerializeObject(v_request);
            DateTime dt_request = DateTime.Now;
            le.request_time = DateTime.Now.ToString("yyyyMMdd HHmmss.fff");
            le.customer_code = v_request.customer_no;
            le.voucher_code = v_request.lg_number;
            le.timestamp = DateTime.Now;
            int row = 81;
            string msss = "";
            try
            {
                Voucher_ResponseModel rrm = new Voucher_ResponseModel();
                rrm.status_code = "000";
                rrm.discount = 0;
                rrm.msg = "Calculate";
                row = 88;

                #region Validate request
                string? v_token_setting = Configuration["AppSettings:v_token"];
                var headers = HttpContext.Request.Headers;
                if (headers.TryGetValue("v_token", out var header_token)) { }
                if (headers.TryGetValue("v_tran_id", out var header_token_v_tran_id)) {
                    le.v_tran_id = header_token_v_tran_id;
                }
                if (headers.TryGetValue("vendor_code", out var header_token_vendor_code)) { }

                try
                {
                    le.header = JsonConvert.SerializeObject(headers); 
                }
                catch (Exception)
                {
                }

                if (string.IsNullOrEmpty(v_token_setting))
                    return StatusCode((int)HttpStatusCode.Unauthorized, new { msg = "v-token setting is null" });

                if (string.IsNullOrEmpty(header_token))
                    return StatusCode((int)HttpStatusCode.Unauthorized, new { msg = "v-token is null" });

                if (!v_token_setting.Equals(header_token))
                    return StatusCode((int)HttpStatusCode.Unauthorized, new { msg = "v-token header_token invalid" });

                if (string.IsNullOrEmpty(v_request.customer_no))
                {
                    rrm.status_code = "001";
                    rrm.msg = "Thông tin mã khách hàng không được trống!";
                }
                if (string.IsNullOrEmpty(v_request.store_no))
                {
                    rrm.status_code = "002";
                    rrm.msg = "Thông tin mã kho không được trống!";
                }
                if (v_request.item == null || v_request.item.Count() == 0)
                {
                    rrm.status_code = "003";
                    rrm.msg = "Danh sách sản phẩm của đơn hàng không được trống!";
                }
                if (string.IsNullOrEmpty(v_request.lg_number))
                {
                    rrm.status_code = "004";
                    rrm.msg = "Thông tin mã voucher không được trống!";
                }

                RestResponse r_voucher = await lgapi.GetVoucherByLocation(v_request.customer_no, v_request.lg_number, v_request.store_no);
                if (r_voucher.StatusCode != HttpStatusCode.OK)
                {
                    rrm.status_code = "023";
                    //rrm.msg = "Voucher " + v_request.lg_number + " ko thuộc khách hàng này hoặc ko áp dụng cho cửa hàng này hoặc đã sử dụng hoặc hết hạn!";
                    LGVoucherModel lg = new LGVoucherModel();
                    if (!string.IsNullOrEmpty(r_voucher.Content))
                    {
                        lg.description = r_voucher.Content;
                        rrm.msg = "[LG]" + "[Cus:" + v_request.customer_no + ",vcode:" + v_request.lg_number + ",store:" + v_request.store_no + "]" + "Error:" + r_voucher.StatusCode.ToString() + "/" + r_voucher.Content;
                    }
                    rrm.v_info = lg;
                }

                // Trả lỗi nếu validate không thành công
                if (rrm.status_code != "000")
                {
                    le.response_time = DateTime.Now.ToString("yyyyMMdd HHmmss.fff");
                    le.response = JsonConvert.SerializeObject(rrm);
                    le.status_code = (int)HttpStatusCode.OK;
                    esearch.IndexDocument(le);
                    return Ok(rrm);
                }
                #endregion

                #region Valiadte Datain

                List<LGVoucherModel>? l_voucher = new List<LGVoucherModel>();
                string? partner_id = "";
                string? funded = "";
                //String redis_voucher_list = "";
                if (_redisclient != null)
                {
                    //l_voucher = _redisclient.Get<List<LGVoucherModel>>("LG_VLIST_BYCUS" + v_request.customer_no);
                }
                //if (string.IsNullOrEmpty(redis_voucher_list))
                if (l_voucher == null || l_voucher.Count == 0)
                {
                    #region old
                    //row = 151;
                    //RestResponse rp = await lgapi.GetListVoucher(v_request.customer_no,null);
                    //row = 152;
                    //if (rp != null)
                    //{
                    //    if (rp.StatusCode == HttpStatusCode.OK)
                    //    {
                    //        if (!string.IsNullOrEmpty(rp.Content) && rp.Content != null)
                    //        {
                    //            row = 160; msss = rp.Content;
                    //            l_voucher = JsonConvert.DeserializeObject<VoucherRoot>(rp.Content).data.ToList();
                    //            if (l_voucher == null || l_voucher.Count == 0)
                    //            {
                    //                row = 164;
                    //                rrm.status_code = "005";
                    //                rrm.msg = "Danh sách voucher của khách hàng rỗng!";
                    //            }
                    //            else if (_redisclient != null)
                    //            {
                    //                //_redisclient.Add("LG_VLIST_CUS" + v_request.customer_no, rp.Content, DateTime.Now.AddMinutes(1));
                    //                _redisclient.Add("LG_VLIST_BYCUS" + v_request.customer_no, l_voucher, DateTime.Now.AddMinutes(20));
                    //            }
                    //        }
                    //        else
                    //        {
                    //            rrm.status_code = "010";
                    //            rrm.msg = "Danh sách voucher của khách hàng đang rỗng!";
                    //        }
                    //    }
                    //    else
                    //    {
                    //        rrm.status_code = "007";
                    //        rrm.msg = "Lỗi lấy danh sách voucher." + rp.ErrorMessage;
                    //    }
                    //}
                    //else
                    //{
                    //    rrm.status_code = "006";
                    //    rrm.msg = "Không kết nối được hệ thống voucher LG!";
                    //}
                    #endregion
                    #region new paging
                    try
                    {
                        l_voucher = await lgapi.GetListVoucherAllPage(v_request.customer_no);
                        if (l_voucher == null || l_voucher.Count == 0)
                        {
                            row = 164;
                            rrm.status_code = "005";
                            rrm.msg = "Danh sách voucher của khách hàng rỗng!";
                        }
                        else if (_redisclient != null)
                        {
                            //_redisclient.Add("LG_VLIST_CUS" + v_request.customer_no, rp.Content, DateTime.Now.AddMinutes(1));
                            _redisclient.Add("LG_VLIST_BYCUS" + v_request.customer_no, l_voucher, DateTime.Now.AddMinutes(20));
                        }
                    }
                    catch (Exception ex)
                    {
                        rrm.status_code = "007";
                        rrm.msg = "Lỗi lấy danh sách voucher." + ex.Message;
                        //goto FaildDataIn;
                    }

                    #endregion
                }
                //else
                //{
                //    l_voucher = JsonConvert.DeserializeObject<VoucherRoot>(redis_voucher_list).data.ToList();
                //}
                LGVoucherModel? v_info = new LGVoucherModel();
                List<ArticleInfoModel> l_article_apply_schema = new List<ArticleInfoModel>();
                if (l_voucher != null && l_voucher.Count > 0)
                {
                    v_info = (from p in l_voucher where p.code == v_request.lg_number select p).FirstOrDefault();

                    string _listarticle = "";
                    List<string> l_cate = new List<string>();
                    string _list_own_brand = "";
                    string _list_supplier_brand = "";
                    string _list_supplier = "";

                    if (v_info != null && v_info.product_ids != null && v_info.product_ids.Count > 0)
                    {
                        _listarticle = string.Join(";", v_info.product_ids);
                    }

                    if (v_info != null && v_info.features != null && v_info.features.Count > 0)
                    {
                        foreach (var feature in v_info.features)
                        {
                            if (feature.taxonomy_slug.Equals("own_brand"))
                            {
                                _list_own_brand += feature.id.ToString() + "_";
                            }
                            if (feature.taxonomy_slug.Equals("supplier_brand"))
                            {
                                _list_supplier_brand += feature.id.ToString() + "_";
                            }
                            if (feature.taxonomy_slug.Equals("supplier"))
                            {
                                _list_supplier += feature.id.ToString() + "_";
                            }
                        }
                    }


                    l_cate.Add(_listarticle); l_cate.Add(_list_own_brand); l_cate.Add(_list_supplier_brand); l_cate.Add(_list_supplier);

                    var _item_has_value = (from p in l_cate where !string.IsNullOrEmpty(p) select p).ToList();
                    if (_item_has_value != null && _item_has_value.Count >= 2)
                    {
                        rrm.status_code = "016";
                        rrm.msg = "Voucher schema không hợp lệ!";
                    }
                    if (_item_has_value == null || _item_has_value.Count ==0)
                    {
                        rrm.status_code = "017";
                        rrm.msg = "Voucher schema bị trống!";
                    }


                    if (!string.IsNullOrEmpty(_listarticle) || !string.IsNullOrEmpty(_list_own_brand) || !string.IsNullOrEmpty(_list_supplier_brand) || !string.IsNullOrEmpty(_list_supplier))
                    {
                        string Key_Schema = "LG_" + LGAPI.CalculateHMACSHA256("Ar:" + _listarticle + "_" + "LG_SM_own_brand:" + _list_own_brand + "_supplier_brand:" + _list_supplier_brand + "_supplier:" + _list_supplier, "MM2024");
                        String redis_ar_list_schema = "";
                        if (_redisclient != null)
                            redis_ar_list_schema = _redisclient.Get<string>(Key_Schema);
                        if (string.IsNullOrEmpty(redis_ar_list_schema))
                        {

                            using (DataTable ar_list_schema = aService.GetArticleList(_listarticle, _list_own_brand, _list_supplier_brand, _list_supplier))
                            {
                                if (ar_list_schema != null && ar_list_schema.Rows.Count > 0)
                                {
                                    l_article_apply_schema = (from p in ar_list_schema.AsEnumerable() select new ArticleInfoModel { Article_code_01 = p.Field<int>("Article_code_01"), Designation_product = p.Field<string>("Designation_product"), supplier_code = p.Field<string>("supplier_code") }).ToList();
                                    if (_redisclient != null)
                                        _redisclient.Add(Key_Schema, JsonConvert.SerializeObject(l_article_apply_schema), DateTime.Now.AddHours(1));
                                }
                                else
                                {
                                    rrm.status_code = "005";
                                    rrm.msg = "Danh sách sản phẩm rỗng!";
                                }
                            }
                        }
                        else
                        {
                            l_article_apply_schema = JsonConvert.DeserializeObject<List<ArticleInfoModel>>(redis_ar_list_schema);
                        }
                    }
                }
                else
                {
                    rrm.status_code = "008";
                    rrm.msg = "Mã voucher không tồn tại cho khách hàng này!";
                }

                if (v_info != null)
                {
                    string Key_Schema_PF = ("LG_PF_" + v_request.customer_no + "_" + v_info.code);
                    String Value_Key_Schema_PF = "";
                    if (_redisclient != null)
                        Value_Key_Schema_PF = _redisclient.Get<string>(Key_Schema_PF);
                    if (!string.IsNullOrEmpty(Value_Key_Schema_PF))
                    {
                        partner_id = Value_Key_Schema_PF.Split(':')[0];
                        funded = Value_Key_Schema_PF.Split(':')[1];
                    }
                    else
                    {
                        using (DataTable v_list = aService.GetVoucherByCustomer(v_request.customer_no, v_info.code))
                        {
                            if (v_list != null && v_list.Rows != null && v_list.Rows.Count > 0)
                            {
                                partner_id = v_list.Rows[0]["partner_id"].ToString();
                                funded = v_list.Rows[0]["funded"].ToString();
                                if (_redisclient != null)
                                    _redisclient.Add(Key_Schema_PF, partner_id + ":" + funded, DateTime.Now.AddHours(1));
                            }
                            else
                            {
                                rrm.status_code = "018";
                                rrm.msg = "Lỗi dữ liệu voucher không đồng bộ!";
                            }
                        }
                    }


                    if (string.IsNullOrEmpty(funded))
                    {
                        rrm.status_code = "019";
                        rrm.msg = "Voucher schema không hợp lệ!";
                    }
                    else
                    {
                        //true = Voucher do nhà cung cấp trả; :false = MM đầu tư.
                        if (funded.ToLower() == "true" && string.IsNullOrEmpty(partner_id))
                        {
                            rrm.status_code = "020";
                            rrm.msg = "Voucher schema nhà cung cấp không hợp lệ!";
                        }
                        if (funded.ToLower() == "false" && !string.IsNullOrEmpty(partner_id))
                        {
                            rrm.status_code = "021";
                            rrm.msg = "Voucher schema nhà cung cấp chi phí không hợp lệ!";
                        }
                        if (!string.IsNullOrEmpty(partner_id) && partner_id.Contains(","))
                        {
                            rrm.status_code = "022";
                            rrm.msg = "Voucher schema số lượng nhà cung cấp không hợp lệ!";
                        }
                    }

                }
                else
                {
                    rrm.status_code = "008";
                    rrm.msg = "Mã voucher không tồn tại cho khách hàng này!";
                }




                if (rrm.status_code != "000")
                {
                    le.response_time = DateTime.Now.ToString("yyyyMMdd HHmmss.fff");
                    le.response = JsonConvert.SerializeObject(rrm);
                    le.status_code = (int)HttpStatusCode.OK;
                    esearch.IndexDocument(le);
                    return Ok(rrm);
                }
                #endregion

                #region Caculate
                rrm.v_tran_id = header_token_v_tran_id!;
                rrm.msg = "ok";

                List<Order_detail_Request> l_detail = v_request.item!.ToList();


                List<Order_detail_Response> l_detail_respon = new List<Order_detail_Response>();
                foreach (var item in l_detail)
                {
                    Order_detail_Response odm = new Order_detail_Response();
                    odm.sq_index = item.sq_index;
                    odm.article_no = item.article_no;
                    odm.price = item.price;
                    odm.qty = item.qty;
                    odm.amt_in_vat = item.amt_in_vat;
                    var a_info = (from p in l_article_apply_schema where p.Article_code_01 == Convert.ToInt32(item.article_no) select p).FirstOrDefault();
                    if (a_info != null)
                    {
                        odm.use_in_voucher = true;
                        odm.supplier_code = a_info.supplier_code ?? "";
                    }
                    else
                        odm.use_in_voucher = false;
                    l_detail_respon.Add(odm);
                }


                if (v_info != null)
                {
                    if (v_info.status == "pending")
                    {
                        if (string.IsNullOrEmpty(v_info.available_from) || string.IsNullOrEmpty(v_info.available_to))
                        {
                            rrm.status_code = "014";
                            rrm.msg = "Ngày bắt đầu hoặc ngày kết thúc của voucher trống!";
                        }
                        else
                        {
                            DateTime dt_fromdate = Convert.ToDateTime(v_info.available_from);
                            DateTime dt_todate = Convert.ToDateTime(v_info.available_to);

                            if (dt_fromdate.Date <= DateTime.Now.Date && dt_todate.Date >= DateTime.Now.Date)
                            {
                                if (l_article_apply_schema != null && l_article_apply_schema.Count > 0)
                                    l_detail = lgapi.ListArticleInOrderActiveVoucher(l_detail, (from p in l_article_apply_schema select p.Article_code_01).ToList());
                                if (l_detail != null && l_detail.Count > 0)
                                {
                                    #region Percent
                                    #region Giảm xx% cho đơn hàng khi mua tối thiểu xy số lượng sp cùng loại, giới hạn giảm giá tối đa xz số lượng sản phẩm
                                    //if (v_info.discount_type.Equals("percent") && v_info.min_units != null && v_info.max_units != null) //&& v_info.product_ids != null)
                                    //{
                                    //    decimal total_qty = (from p in l_detail select p.qty).Sum();
                                    //    if (total_qty >= v_info.min_units)
                                    //    {
                                    //        if (total_qty >= v_info.max_units)
                                    //            total_qty = v_info.max_units.Value;

                                    //        decimal total_amount = total_qty * l_detail[0].price;
                                    //        rrm.discount = total_amount * (decimal)v_info.discount;
                                    //    }
                                    //}

                                    #endregion
                                    #region Giảm xx % cho đơn hàng theo [Product or own_brand or supplier_brand or Supplier] không giới hạn tối đa giảm giá
                                    if (v_info.discount_type.Equals("percent") && v_info.max_money == null && v_info.min_units == null)
                                    {
                                        decimal total_amount = (from p in l_detail select p.amt_in_vat).Sum();
                                        decimal totall_discount = total_amount * (decimal)v_info.discount;
                                        rrm.discount = totall_discount;
                                    }
                                    #endregion
                                    #region Giảm xx % cho đơn hàng theo [Product or own_brand or supplier_brand or Supplier], giảm tối đa xy VND
                                    else if (v_info.discount_type.Equals("percent") && v_info.max_money != null)
                                    {
                                        decimal total_amount = (from p in l_detail select p.amt_in_vat).Sum();
                                        decimal totall_discount = total_amount * (decimal)v_info.discount;
                                        rrm.discount = totall_discount >= v_info.max_money ? v_info.max_money : totall_discount;
                                    }
                                    #endregion
                                    #endregion

                                    #region Cash
                                    //Giảm giá xx VND cho đơn hàng tối thiếu mua xy VND theo [Product or own_brand or supplier_brand or Supplier]
                                    else if (v_info.discount_type.Equals("cash") && v_info.min_money != null)
                                    {
                                        decimal total_amount = (from p in l_detail select p.amt_in_vat).Sum();
                                        rrm.discount = total_amount >= v_info.min_money ? (decimal)v_info.discount : 0;
                                    }
                                    //Giảm giá xx VND cho đơn hàng theo [Product or own_brand or supplier_brand or Supplier]
                                    else if (v_info.discount_type.Equals("cash") && v_info.min_money == null)
                                    {
                                        decimal total_amount = (from p in l_detail select p.amt_in_vat).Sum();
                                        rrm.discount = (decimal)v_info.discount;
                                    }
                                    #endregion

                                    else
                                    {
                                        rrm.status_code = "011";
                                        rrm.msg = "Schema voucher không được khai báo trên hệ thống tính toán!";
                                    }
                                }
                                else
                                {
                                    rrm.status_code = "012";
                                    rrm.msg = "Không có sản phẩm nào phù hợp với rule giảm giá của Voucher!";
                                }
                            }
                            else
                            {
                                rrm.status_code = "015";
                                rrm.msg = "Voucher đã hết hạn sử dụng!";
                            }
                        }
                    }
                    else
                    {
                        rrm.status_code = "013";
                        rrm.msg = "Trạng thái voucher không thể sử dụng!";
                    }
                }
                else
                {
                    rrm.status_code = "008";
                    rrm.msg = "Mã voucher không tồn tại cho khách hàng này!";
                }

                #endregion

                rrm.item = l_detail_respon.ToArray();
                v_info.supplier_code = partner_id;

                //Old
                //if (l_detail_respon != null && l_detail_respon.Count > 0)
                //{
                //    foreach (Order_detail_Response item in l_detail_respon)
                //    {
                //        if (!string.IsNullOrEmpty(item.supplier_code))
                //        {
                //            v_info.supplier_code = item.supplier_code;
                //            break;
                //        }
                //    }
                //}

                rrm.v_info = v_info;
                DateTime dt_response = DateTime.Now;
                le.response_time = dt_response.ToString("yyyyMMdd HHmmss.fff");

                try
                {
                    TimeSpan timeDifference = dt_response - dt_request;
                    double millisecondsDifference = timeDifference.TotalMilliseconds;
                    le.timestamp_mili = (int)millisecondsDifference;
                }
                catch (Exception)
                {
                }
                le.response = JsonConvert.SerializeObject(rrm);
                //le.response = rrm;
                le.status_code = (int)HttpStatusCode.OK;
                esearch.IndexDocument(le);
                return Ok(rrm);
            }
            catch (Exception ex)
            {
                le.response_time = DateTime.Now.ToString("yyyyMMdd HHmmss.fff");
                le.response = "row:" + row.ToString() + "|" + ex.Message;
                le.status_code = (int)HttpStatusCode.InternalServerError;
                esearch.IndexDocument(le);
                return StatusCode((int)HttpStatusCode.InternalServerError, new { msg = ex, row = row, contt = msss });
            }
        }

        [HttpPost("redeem")]
        public async Task<ActionResult> redeem(string customer_no, string lg_number)
        {
            List<LGVoucherModel>? l_voucher = new List<LGVoucherModel>();
            if (_redisclient != null)
            {
                l_voucher = _redisclient.Get<List<LGVoucherModel>>("LG_VLIST_BYCUS" + customer_no);
                if (l_voucher != null && l_voucher.Count > 0)
                {
                    LGVoucherModel lg = (from p in l_voucher where p.code == lg_number select p).FirstOrDefault();
                    if (lg != null)
                    {
                        _redisclient.Remove("LG_VLIST_BYCUS" + customer_no);
                        l_voucher.Remove(lg);
                        _redisclient.Add("LG_VLIST_BYCUS" + customer_no, l_voucher, DateTime.Now.AddMinutes(20));
                        return Ok();
                    }
                    else
                        return Ok();
                }
                else
                    return Ok();
            }
            else
                return Ok();
        }
    }
}
