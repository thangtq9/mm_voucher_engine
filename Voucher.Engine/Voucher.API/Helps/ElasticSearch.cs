﻿using Nest;
using Newtonsoft.Json;
using static ServiceStack.Diagnostics.Events;

namespace Voucher.API.Helps
{
    public class ElasticSearch
    {
        protected readonly IConfiguration Configuration;
        private readonly ElasticClient _client;
        private string path_log;
        private bool ping_status;
        public ElasticSearch(IConfiguration configuration)
        {
            String currentMonth = DateTime.Now.ToString("yyyyMM");
            String url = GetAppSetting(configuration, "ElasticSearchIP");// "http://172.26.16.109:9200";
            String username = GetAppSetting(configuration, "ElasticUser");// "aspiron";
            String password = GetAppSetting(configuration, "ElasticPassWord"); //"P@ssw0rd";
            path_log = GetAppSetting(configuration, "Path_Log"); //"P@ssw0rd";
            String index = "voucher-mm-v2-log-" + currentMonth;
            var uri = new Uri(url);
            var settings = new ConnectionSettings(uri);
            settings.BasicAuthentication(username, password);
            settings.RequestTimeout(TimeSpan.FromMilliseconds(500));
            _client = new ElasticClient(settings);
            // Thử kết nối Elasticsearch
            var pingResponse = _client.Ping();
            ping_status = pingResponse.IsValid;
            settings.DefaultIndex(index);
        }
        public static string GetAppSetting(IConfiguration configuration, string key)
        {
            string _key = "";
            try
            {
                return configuration["AppSettings:" + key].ToString();
            }
            catch (Exception ex)
            {
                _key = "-10";
            }
            return _key;
        }

        public bool IndexDocument<TDocument>(TDocument document) where TDocument : class
        {
            if (ping_status == true)
            {
                var indexResponse = _client.IndexDocument(document);
                return indexResponse.IsValid;
            }
            else
            {
                string filename = "voucher_" + DateTime.Now.ToString("yyyyMMdd");
                string Log = System.Environment.NewLine + JsonConvert.SerializeObject(document);
                FileStream _stream = null;
                StreamWriter sw = null;
                try
                {
                    string Path = path_log + "/" + filename + ".log";

                    if (!File.Exists(Path))
                        _stream = File.Create(Path);
                    else
                    {
                        FileInfo file = new FileInfo(Path);
                        if (file.Length > 20000000)
                        {
                            file.Delete();
                            _stream = File.Create(Path);
                        }
                        else
                            _stream = new FileStream(Path, FileMode.Append);
                    }
                    sw = new StreamWriter(_stream);
                    sw.Write("***" + DateTime.Now.ToString() + Environment.NewLine + Log + Environment.NewLine);
                    sw.Flush();
                }
                catch (Exception ex)
                {
                }
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
                if (_stream != null)
                {
                    _stream.Close();
                    _stream.Dispose();
                }
                return true;
            }
        }
    }
}
